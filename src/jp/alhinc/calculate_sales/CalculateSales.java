package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	private static final String FILE_POPNAME_BRANCH = "支店定義ファイル";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	private static final String FILE_POPNAME_COMMODITY = "商品定義ファイル";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// 支店コードフォーマット
	private static final String BRANCH_NAME_FORMAT = "^[0-9]{3}$";

	// 商品コードフォーマット
	private static final String COMMODITY_NAME_FORMAT = "^[a-z0-9]{8}$";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String EARNING_SERIALNUMBER_ERROR = "売上ファイル名が連番になっていません";
	private static final String FORMAT_ERROR = "のフォーマットが不正です";
	private static final String EARNING_INVALID_BRANCH_CODE = "の支店コードが不正です";
	private static final String EARNING_INVALID_COMMODITY_CODE = "の商品コードが不正です";
	private static final String EARNING_TENDIGITS_OVER = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String,String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String,Long> commoditySales = new HashMap<>();

		//コマンドライン引数が渡されているかを確認する
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店・商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,
				BRANCH_NAME_FORMAT, FILE_POPNAME_BRANCH) ||
				!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,
						COMMODITY_NAME_FORMAT, FILE_POPNAME_COMMODITY)) {
			return;
		}

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		BufferedReader br = null;

		//フォルダの中から売上ファイルだけを抽出
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles);

		//売上ファイルの性質を確認する
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			//売上ファイルが連番になっているか確認する
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if(latter - former != 1) {
				System.out.println(EARNING_SERIALNUMBER_ERROR);
				return;
			}

		}

		//売上金額を抽出して保持した額に足す
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);
				String salesLines;
				List<String> lines = new ArrayList<>();

				while((salesLines = br.readLine()) != null) {
					lines.add(salesLines);
				}

				//支店コード、商品コードをそれぞれ変数に入れておく。
				String branchCode = lines.get(0);
				String commodityCode = lines.get(1);

				//売上ファイルの中身が3行を超えていないかを確認する
				if(lines.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FORMAT_ERROR);
					return;
				}

				//売上ファイルの支店コードが支店定義ファイルに存在するか確認する
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(rcdFiles.get(i).getName() + EARNING_INVALID_BRANCH_CODE);
					return;
				}

				//売上ファイルの商品コードが商品定義ファイルに存在するか確認する
				if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(rcdFiles.get(i).getName() + EARNING_INVALID_COMMODITY_CODE);
					return;
				}

				//売上金額が数字であることを確認する
				if(!lines.get(2).matches("^[0-9]+$")){
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//売上金額を取り出し、支店別集計ファイルに加算する
				long fileSale = Long.parseLong(lines.get(2));
				Long salesAmount = branchSales.get(branchCode) + fileSale;

				//売上金額を取り出し、商品別集計ファイルに加算する
				Long comSalesAmount = commoditySales.get(commodityCode) + fileSale;

				//支店別または商品別売上金額の合計が10桁を超えていないか確認する
				if(salesAmount >= 10000000000L || comSalesAmount >= 10000000000L) {
					System.out.println(EARNING_TENDIGITS_OVER);
					return;
				}

				//支店別集計ファイルを上書きする
				branchSales.put(branchCode,salesAmount);

				//賞品別集計ファイルを上書きする
				commoditySales.put(commodityCode, comSalesAmount);


			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
			}

		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		//商品別集計ファイル書き込み処理
		if(!writeFile(args[0],FILE_NAME_COMMODITY_OUT,commodityNames,commoditySales)) {
			return;
		}
	}

	/**
	 * 支店・商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> mapNames,
			Map<String, Long> mapSales, String fileNameFormat, String popName) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			//ファイルの有無確認。ファイル名を引数で受けて一つにまとめたい
			if(!file.exists()) {
				System.out.println(popName + FILE_NOT_EXIST);
				return false;

			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				if((items.length != 2) || !(items[0].matches(fileNameFormat))) {
					System.out.println(popName + FORMAT_ERROR);
					return false;
				}

				mapNames.put(items[0], items[1])	;
				mapSales.put(items[0], 0L);

			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;

				}
			}
		}
		return true;
	}

	/**
	 * 支店別・商品別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> mapNames, Map<String, Long> mapSales) {
//		ファイル書き込み処理 支店別集計ファイル出力、商品別集計ファイル出力
		BufferedWriter bw = null;

		try {
			File earn = new File(path, fileName);
			FileWriter fw = new FileWriter(earn);
			bw = new BufferedWriter(fw);

			for(String key : mapNames.keySet()) {
				String names = mapNames.get(key);
				String sales = String.valueOf(mapSales.get(key));

				bw.write(key + "," + names + "," + sales);
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			try {
				bw.close();
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return false;
			}

		}
		return true;
	}
}